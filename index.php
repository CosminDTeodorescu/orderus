<?php

require 'vendor/autoload.php';

use Game\Game as Game;

$game = new Game();
$game->play();