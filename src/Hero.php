<?php

namespace Game;

class Hero extends Character
{
    public function __construct($attributes)
    {
        parent::__construct($attributes);
    }

    protected function damageOnAttackTrigger(&$damage)
    {
        if ($this->isAttacker()) {
            foreach ($this->skills as $skill) {
                if ($skill->getSkillType() == "offence") {
                    $damage = $skill->activate($damage);
                }
            }
        }

        return $damage;
    }

    protected function damageOnDefendTrigger(&$damage)
    {
        if (!$this->isAttacker()) {
            foreach ($this->skills as $skill) {
                if ($skill->getSkillType() == "defence") {
                    $damage = $skill->activate($damage);
                }
            }
        }

        return $damage;
    }

    private function isAttacker()
    {
        return (bool)parent::getAttribute('isAttacking');
    }
}