<?php

return [
    'orderus' => [
        'name'        => 'Orderus',
        'health'      => rand(70, 100),
        'strength'    => rand(70, 80),
        'defence'     => rand(45, 55),
        'speed'       => rand(40, 50),
        'luck'        => rand(10, 30)
    ],
    'beast' => [
        'name'        => 'Beast',
        'health'      => rand(60, 90),
        'strength'    => rand(60, 60),
        'defence'     => rand(40, 60),
        'speed'       => rand(40, 60),
        'luck'        => rand(25, 40)
    ],
];
