<?php


namespace Game\Skills;


class MagicShield extends Skill
{
    public function __construct()
    {
        $this->setName('Magic Shield');
        $this->setDescription('Defender will take half the damage this turn!');
        $this->setChance(20);
        $this->setSkillType('defence');
    }

    public function activate($damage)
    {
        if ($this->canExecute($this->getChance())) {
            $damage = $damage / 2;

            echo "{$this->getName()} has been activated! {$this->getDescription()} \n";
        }

        return $damage;
    }


}