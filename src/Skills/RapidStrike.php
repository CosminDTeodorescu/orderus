<?php


namespace Game\Skills;


class RapidStrike extends Skill
{
    public function __construct()
    {
        $this->setName('Rapid Strike');
        $this->setDescription('Attacker will strike twice!');
        $this->setChance(10);
        $this->setSkillType('offence');
    }

    public function activate($damage)
    {
        if ($this->canExecute($this->getChance())) {
            $damage = $damage * 2;

            echo "{$this->getName()} has been activated! {$this->getDescription()} \n";
        }

        return $damage;
    }
}