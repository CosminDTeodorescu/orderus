<?php


namespace Game\Skills;


abstract class Skill
{
    protected $name;
    protected $description;
    protected $chance;
    protected $skill_type;

    abstract protected function activate($damage);

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getChance()
    {
        return $this->chance;
    }

    /**
     * @param mixed $chance
     */
    public function setChance($chance)
    {
        $this->chance = $chance;
    }

    /**
     * @return mixed
     */
    public function getSkillType()
    {
        return $this->skill_type;
    }

    /**
     * @param mixed $skill_type
     */
    public function setSkillType($skill_type)
    {
        $this->skill_type = $skill_type;
    }

    protected function canExecute($chance)
    {
        return $chance >= rand(1,100);
    }

}