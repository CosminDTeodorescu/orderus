<?php

namespace Game;

class Beast extends Character
{
    protected function damageOnAttackTrigger(&$damage)
    {
        return $damage;
    }

    protected function damageOnDefendTrigger(&$damage)
    {
        return $damage;
    }
}