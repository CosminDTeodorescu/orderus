<?php

namespace Game;

abstract class Character
{
    protected $name;
    protected $health;
    protected $strength;
    protected $defence;
    protected $speed;
    protected $luck;
    protected $isAttacking;
    protected $skills;

    abstract protected function damageOnAttackTrigger(&$damage);
    abstract protected function damageOnDefendTrigger(&$damage);

    public function __construct($attributes)
    {
        foreach ($attributes as $key => $attribute) {
            $this->{$key} = $attribute;
        }
        $this->isAttacking = false;
        $this->skills = [];
    }

    /**
     * Display Initial stats for this character
     */
    public function showStats()
    {
        echo $this . PHP_EOL;
    }

    public function __toString()
    {
        return "HP: {$this->health} || DMG: {$this->strength} || DEF: {$this->defence} || SPEED: {$this->speed} || LUCK: {$this->luck} \n";
    }

    /**
     * Display all skills for this character
     */
    public function showSkills()
    {
        $skills           = $this->getAttribute('skills');
        $number_of_skills = count($skills);

        if ($number_of_skills == 0) {
            echo "{$this->getAttribute('name')} has no skills \n";
        } else {
            echo "{$this->getAttribute('name')} has the following skills: ";

            foreach ($skills as $key => $skill) {
                $comma = (($key + 1) < $number_of_skills) ? ', ' : ' ';
                echo $skill->getName() . $comma;
            }
            echo "\n";
        }
    }

    public function getAttribute($attribute)
    {
        return $this->{$attribute};
    }

    public function setHealth($health)
    {
        $this->health = $health;
    }

    public function addSkill($skill)
    {
        array_push($this->skills, $skill);
    }

    public function setIsAttacking($val)
    {
        $this->isAttacking = (bool) $val;
    }

    public function attack(Character $player)
    {
        echo "{$this->getAttribute('name')} is attacking {$player->getAttribute('name')} \n";

        $initial_attack_damage  = $this->getAttribute('strength') - $player->getAttribute('defence');
        $final_attack_damage    = $this->damageOnAttackTrigger($initial_attack_damage);

        echo "{$this->getAttribute('name')} deals $final_attack_damage DMG to {$player->getAttribute('name')}\n";

        $player->defend($final_attack_damage);
    }

    private function defend($damage)
    {
        $final_damage = $this->damageOnDefendTrigger($damage);

        $this->setHealth($this->getAttribute('health') - $final_damage);

        $remaining_health = ($this->getAttribute('health') > 0) ? $this->getAttribute('health') : 0;

        echo "{$this->getAttribute('name')} has $remaining_health HP remaining. \n";
    }

}