<?php

namespace Game;

use Game\Hero;
use Game\Beast;
use Game\Skills\MagicShield;
use Game\Skills\RapidStrike;

class Game
{
    private $config;
    private $players;
    private $player1;
    private $player2;
    private $attacking_player;
    private $defending_player;
    private $current_round;
    private $winner;
    private $game_over;

    const NO_OF_ROUNDS = 20;

    public function __construct()
    {
        $this->config = include 'config.php';

        $this->current_round = 0;
        $this->game_over     = false;

        $this->player1 = new Hero($this->config['orderus']);
        $this->player1->addSkill(new MagicShield());
        $this->player1->addSkill(new RapidStrike());

        $this->player2 = new Beast($this->config['beast']);

        $this->players = [
            &$this->player1,
            &$this->player2
        ];

        $this->winner = null;
    }

    /**
     * Run the game
     *
     * @return void
     */
    public function play()
    {
        $this->presentPlayers();

        $this->getInitialPlayingOrder($this->players);

        $this->attacking_player = $this->players[0];
        $this->attacking_player->setIsAttacking(true);

        $this->defending_player = $this->players[1];

        while ($this->current_round < self::NO_OF_ROUNDS && $this->winner == null) {
            $this->playRound();

            if ($this->isGameOver()) {
                echo "Game Over";
                break;
            }

            $this->switchPlayers();
            $this->incrementRound();

        }

    }

    /**
     * Initial greeting message
     *
     * @return void
     */
    private function presentPlayers()
    {
        echo "\e[1;32mSTART GAME\e[0m \n";

        foreach ($this->players as $key => $player) {
            $name = $player->getAttribute('name');

            echo "$name has entered the battlefield! \n";

            $player->showStats();
            $player->showSkills();
            $this->printSeparator();
        }
    }

    /**
     * Sort the array of players in descending order
     * by comparing their speed and luck attributes.
     * Thus, the first element of the array will be the
     * starting player
     *
     * @param $players
     */
    private function getInitialPlayingOrder(&$players) {
        usort($players, function ($a, $b) use (&$players) {

            if ($a->getAttribute('speed') == $b->getAttribute('speed')) {
                return ($a->getAttribute('luck') > $b->getAttribute('luck')) ? -1 : 1;
            }

            return ($a->getAttribute('speed') > $b->getAttribute('speed')) ? -1 : 1;
        });
    }

    /**
     * Play out the current round
     *
     * @return void
     */
    private function playRound()
    {
        $this->displayCurrentRound();

        if ($this->current_round == 0) {
            echo "{$this->attacking_player->getAttribute('name')} goes first \n";
        }

        $this->attacking_player->attack($this->defending_player);
        $this->printSeparator();
    }

    /**
     * When the defending player loses all health
     * the game is over
     *
     * @return bool
     */
    private function isGameOver()
    {
        if ($this->defending_player->getAttribute('health') <= 0) {
            $this->winner = $this->attacking_player;

            echo "{$this->winner->getAttribute('name')} is the winner! \n";

            return true;
        } elseif(($this->current_round + 1) == self::NO_OF_ROUNDS) {
            return true;
        }

        return false;
    }

    /**
     * Switch the players for the new round
     * The attacking player becomes the defending player
     * and vice-versa
     */
    private function switchPlayers()
    {
        $this->players = array_reverse($this->players);
        $this->attacking_player = $this->players[0];
        $this->attacking_player->setIsAttacking(true);

        $this->defending_player = $this->players[1];
        $this->defending_player->setIsAttacking(false);
    }



    private function displayCurrentRound()
    {
        $display_round =  $this->current_round + 1;
        echo "\e[1;34mROUND $display_round\e[0m \n";
    }

    /**
     * Go to next round
     */
    private function incrementRound()
    {
        $this->current_round = $this->current_round + 1;
    }

    private function printSeparator() {
        echo "______________________________________________________________\n";
    }
}